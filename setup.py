# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from setuptools import setup, find_packages


try:
    from pypandoc import convert
except ImportError:
    import io

    def convert(filename, fmt):
        with io.open(filename, encoding='utf-8') as fd:
            return fd.read()


setup(
    name='django-stork',
    version='1.0.3',
    author='Aleksandr Alekseev',
    author_email='alekseevavx@gmail.com',
    description='Приложение для отправки данных в канарейку',
    long_description=convert('README.md', 'rst'),
    url='https://bitbucket.org/fbkteam/stork/',
    license='MIT',
    keywords=['django', 'canary'],
    install_requires=[
        'requests>=2',
        'pydantic>=1',
    ],
    packages=find_packages(),
)
