from django.apps import AppConfig


class StorkConfig(AppConfig):
    name = 'stork'
