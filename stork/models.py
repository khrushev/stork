from typing import List

from django.db import models

from .mixins import ModelDiffMixin
from .services import Contact


class CanarySyncQuerySet(models.QuerySet):
    """QS для работы с моделью данных для канарейки"""
    def to_canary(self) -> List[Contact]:
        """Преобразование queryset-а в модели контактов для отправки в канарейку"""
        raise NotImplementedError()

    def unsynced(self):
        """Возвращает записи, которые должны быть синхронизованы"""
        return self.filter(is_synchronized=False, email_confirmed=True)


class CanarySyncAbstractModel(ModelDiffMixin, models.Model):
    """Модель для синхронизации с канарейкой"""
    # Список полей, при изменении которых буден необходима отправка новых данных в канарейку
    fields_to_canary_resync = None

    email_confirmed = models.BooleanField('подтвердил почту', default=False, db_index=True)
    is_synchronized = models.BooleanField('синхронизирован', default=False, db_index=True)
    created_at = models.DateTimeField('создано', auto_now_add=True)
    modified_at = models.DateTimeField('обновлено', auto_now=True)

    objects = CanarySyncQuerySet.as_manager()

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        fields_to_canary_resync = self.fields_to_canary_resync or []
        is_resync_fields_is_changed = bool(set(fields_to_canary_resync) & set(self.changed_fields))

        if is_resync_fields_is_changed:
            self.is_synchronized = False

        super().save(*args, **kwargs)
