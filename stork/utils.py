from django.core.paginator import Paginator


def queryset_iterator(qs, chunk_size=1000):
    paginator = Paginator(qs.order_by('id'), chunk_size)
    total = paginator.count
    for page_i in paginator.page_range:
        current_page = paginator.get_page(page_i)
        start = (page_i - 1) * chunk_size
        end = min(start + chunk_size, total)
        yield start, end, total, current_page.object_list
